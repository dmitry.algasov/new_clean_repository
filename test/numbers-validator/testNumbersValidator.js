import { NumbersValidator } from '/app/numbers_validator.js';
import { expect } from 'chai';

const assert = require('assert');
const NumbersValidator = require('./numbers_validator.js');


describe('NumbersValidator', () => {
    describe('getEvenNumbersFromArray', () => {
        it('should return an array of even numbers', () => {
            const validator = new NumbersValidator();
            const inputArray = [2, 4, 6, 8];
            const expectedOutput = [2, 4, 6, 8];
            const result = validator.getEvenNumbersFromArray(inputArray);
            assert.deepStrictEqual(result, expectedOutput);
        });

        it('should handle an empty array', () => {
            const validator = new NumbersValidator();
            const inputArray = [];
            const expectedOutput = [];
            const result = validator.getEvenNumbersFromArray(inputArray);
            assert.deepStrictEqual(result, expectedOutput);
        });

        it('should throw an error for non-numeric input in the array', () => {
            const validator = new NumbersValidator();
            const inputArray = [2, 'abc', 6, 'def'];
            assert.throws(() => validator.getEvenNumbersFromArray(inputArray), /is not an array of "Numbers"/);
        });
    });

    describe('isAllNumbers', () => {
        it('should return true for an array of numbers', () => {
            const validator = new NumbersValidator();
            const inputArray = [1, 2, 3, 4];
            const result = validator.isAllNumbers(inputArray);
            assert.strictEqual(result, true);
        });

        it('should return true for an empty array', () => {
            const validator = new NumbersValidator();
            const inputArray = [];
            const result = validator.isAllNumbers(inputArray);
            assert.strictEqual(result, true);
        });

        it('should throw an error for an array containing non-numeric values', () => {
            const validator = new NumbersValidator();
            const inputArray = [1, 'abc', 3, 'def'];
            assert.throws(() => validator.isAllNumbers(inputArray), /is not an array of "Numbers"/);
        });
    });

    describe('isInteger', () => {
        it('should return true for integer values', () => {
            const validator = new NumbersValidator();
            assert.strictEqual(validator.isInteger(5), true);
            assert.strictEqual(validator.isInteger(0), true);
            assert.strictEqual(validator.isInteger(-3), true);
        });

        it('should throw an error for non-numeric input', () => {
            const validator = new NumbersValidator();
            assert.throws(() => validator.isInteger('abc'), /is not a number/);
        });

        it('should return false for non-integer numeric values', () => {
            const validator = new NumbersValidator();
            assert.strictEqual(validator.isInteger(2.5), false);
            assert.strictEqual(validator.isInteger(-0.5), false);
        });
    });
});